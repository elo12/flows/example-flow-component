# example-flow-component

This example shows how to setup a project, only using gradle. To build & deploy an ELO flow component.

This example does not make use of the ELO VS-Code plugin.

## Usage

`./gradlew deployFlow` OR `./gradlew dev`

`deployFlow` is just an alias for the `dev` task.
