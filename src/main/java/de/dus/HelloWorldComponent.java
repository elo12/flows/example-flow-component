package de.dus;

import com.elo.flows.api.components.annotations.Component;
import com.elo.flows.api.components.annotations.Connection;
import com.elo.flows.api.components.annotations.ConnectionRequired;
import com.elo.flows.api.components.annotations.Service;
import com.elo.flows.filestore.FlowFile;
import com.google.common.base.Strings;
import de.elo.ix.client.IXConnection;

@Component(version = "1.0.0-SNAPSHOT", namespace = "de.dus", name = "HelloWorld", displayName = "component.HelloWorld.display.name")
public class HelloWorldComponent {

    @Connection
    IXConnection ixConnect;

    @Service(displayName = "com.elo.flows.helloworld.service")
    public HelloOutput myService(HelloInput obj) {
        if (Strings.isNullOrEmpty(obj.getText())) {
            System.err.println();
        }
        HelloOutput result = new HelloOutput();
        result.setText(obj.getText() + "HI");
        return result;
    }

    @Service(displayName = "com.elo.flows.helloworld.servicewithconnection")
    @ConnectionRequired
    public HelloOutput myServiceWithConnection() {
        HelloOutput result = new HelloOutput();
        result.setText("IX-Version: " + ixConnect.getImplVersion());
        return result;
    }
}